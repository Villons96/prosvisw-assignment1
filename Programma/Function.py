import mysql.connector
from datetime import datetime
import time
import re


# THis function create database (database + table) only if it doesn't exist, in other case does nothing
def create_database():
    # Waits for 20 seconds
    time.sleep(20)
    # Open a connection with my localhost db
    mydb = mysql.connector.connect(host="mysqlcontainer", user="root", passwd="passowrd")
    # Create a cursor on my localhost db
    cursor = mydb.cursor()
    # Create a database if it doesn't exist
    cursor.execute('''CREATE DATABASE IF NOT EXISTS RATINGDB;''')
    cursor.close()
    mydb.close()

    # Open a connection with my localhost db
    mydb = mysql.connector.connect(host="mysqlcontainer", user="root", passwd="passowrd", database='RATINGDB')
    # Create a cursor on my localhost db
    cursor = mydb.cursor()
    # Create a tupla for keep my create table operation
    table_born = (" CREATE TABLE IF NOT EXISTS `Rating` ("
    "  `id` int(11) NOT NULL AUTO_INCREMENT,"
    "  `Name` varchar(14) NOT NULL,"
    "  `Email` varchar(25) NOT NULL,"
    "  `Date` date NOT NULL,"
    "  `Rate` int(2) NOT NULL,"
    "  PRIMARY KEY (`id`)"
    ") ENGINE=InnoDB")
    # Create a table if it doesn't exist
    cursor.execute(table_born)
    cursor.close()
    mydb.close()


# This funciont allows to insert record data into db
def insert_into_db(record):
    # Open a connection with my localhost db
    mydb = mysql.connector.connect(host="mysqlcontainer", user="root", passwd="passowrd", database='RATINGDB')
    # Create a cursor on my localhost db
    cursor = mydb.cursor()
    # Insert data
    cursor.execute("INSERT INTO Rating "
               "(Name, Email, Date, Rate) "
               "VALUES (%s, %s, %s, %s)", record)
    # Impose persistence
    mydb.commit()
    cursor.close()
    mydb.close()


# This function show to console all review stored and compute/show the rating avg
def show_rating_and_avg():
    # Open a connection with my localhost db
    mydb = mysql.connector.connect(host="mysqlcontainer", user="root", passwd="passowrd", database='RATINGDB')
    # Create a cursor on my localhost db
    cursor = mydb.cursor()
    # Select data
    cursor.execute("SELECT Name, Rate, Date FROM Rating")
    count = 0
    amount = 0

    # Impose a good output format
    print('{:>15}  {:>15}  {:>15}'.format("Name", "Rating", "Date"))
    # Iterate all query result
    for row in cursor:
        count += 1
        amount += int(row[1])
        print('{:>15}  {:>15}  {:>15}'.format(row[0], row[1], str(row[2])))

    print("Restaurant rating average is:", round(amount/count, 2))
    mydb.close()
    cursor.close()


# This function check if the user can vote, one user can vote only one time for day
def check_vote_permit(email):
    # Create database if it doesn't exist
    create_database()
    # Open a connection with my localhost db
    mydb = mysql.connector.connect(host="mysqlcontainer", user="root", passwd="passowrd", database='RATINGDB')
    # Create a cursor on my localhost db
    cursor = mydb.cursor()
    # Perform query
    cursor.execute("SELECT Email, Date FROM Rating")

    # Iterate all query result
    for row in cursor:
        # check user permit
        if(row[0] == email.lower()) and (row[1] == datetime.now().date()):
            return False
    return True


# This function check email format, it could be improve
def check_email_format(email):
    if re.search('\S+@\S+', email):
        return True
    return False

# This function check username format, it could be improve
def check_username_format(username):
    not_accepted_symbols = ["&", "@", "/", "\\"] 
    if len(username) < 1 or len(username) > 14:
        return False
    for symbol in not_accepted_symbols:
        if symbol in username:
            return False
    return True

# This function check vote format
def check_vote_format(x):
    # Impose a try/except block for manage bad input format (e.g. string, char)
    try:
        x = int(x)
    except:
        return False

    # Check rating value
    if (x > 10) or (x < 0):
        return False
    return True

