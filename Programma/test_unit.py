from datetime import datetime
import Function


def test_check_email():
    good_emails = ["giacomo@gmail.com", "bruno@libero.it", "lorenzo@icloud.com"]
    bad_emails = ["giacomogmail.com", "1234", "ciaociao"]

    for good_email in good_emails:
        try:
            assert Function.check_email_format(good_email) == True
        except AssertionError:
            print("Error: " + good_email + " is not taken even if is correct")
            quit()

    for bad_email in bad_emails:
        try:
            assert Function.check_email_format(bad_email) == False
        except AssertionError:
            print("Error: " + bad_email + " is taken even if is not correct")
            quit()


def test_check_marks():
    good_marks = ["1", "10", "5"]
    bad_marks = ["-5", "string", "c"]

    for good_mark in good_marks:
        try:
            assert Function.check_vote_format(good_mark) == True
        except AssertionError:
            print("Error: "+good_mark+" is not taken even if is correct")
            quit()

    for bad_mark in bad_marks:
        try:
            assert Function.check_vote_format(bad_mark) == False
        except AssertionError:
            print("Error: "+bad_mark+" is taken even if is not correct")
            quit()


def test_vote_permit():
    users = [('Giacomo', 'email1@gmail.com', datetime.now().date(), '5'),
             ('Bruno', 'email2@gmail.com', datetime.now().date(), '8'),
             ('Stefano', 'email3@gmail.com', datetime.now().date(), '5'),
             ('Lorenzo', 'email4@gmail.com', datetime.now().date(), '9')]

    Function.create_database()

    for user in users:
        Function.insert_into_db(user)

    try:
        assert Function.check_vote_permit("email1@gmail.com") == False
    except AssertionError:
        print("Error: email1@gmail.com is already in db")
        quit()
        
def test_user_permit():
    good_users = ['Giacomo', 'Stefano', 'Lorenzo', 'Fabio','MA42']
    bad_users = ['Bruno&', 'L@lla@', '\era', 'marce//o', 'mari@','giangiacomorivellino']
    for good_user in good_users:
        try:
            assert Function.check_username_format(good_user) == True
        except AssertionError:
            print("Username: "+ good_user +" refused when it's valid")
            quit()
    for bad_user in bad_users:
        try:
            assert Function.check_username_format(bad_user) == False
        except AssertionError:
            print("Username: "+ bad_user +" accepted when it's invalid")
            quit()

print("Start Test")
test_check_email()
print("Test check email passed!")
test_check_marks()
print("Test check marks passed!")
test_vote_permit()
print("Test vote permit passed!")
test_user_permit()
print("Test user permit passed!")





