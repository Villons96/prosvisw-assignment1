from datetime import datetime
import Function


print("Welcome to BruJack restaurant rating system")


# Take in input name and email
name = input("Insert your name: ")
email = input("Insert your mail: ")
vote = input("Insert your vote: ")

# Check user vote format
if not Function.check_vote_format(vote):
    print("Bad vote format")
    quit()

# Check if the user mail is correct
if not Function.check_email_format(email):
    print("Bad email format")
    quit()

# Check if the user can vote
if not Function.check_vote_permit(email):
    print("You can vote only one time per day")
    quit()

# Create a record to insert into db
record = (name, email.lower(), datetime.now().date(), vote)

# Insert data
Function.insert_into_db(record)

# Show all rating and a rating avg
Function.show_rating_and_avg()


