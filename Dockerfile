FROM python:3.6

WORKDIR /myapp

RUN apt-get update
RUN pip install mysql-connector-python 

COPY . /myapp

CMD ["python", "./Programma/Main.py"]
