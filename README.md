# Assignment 1 Processo e Sviluppo Software
### Team
Palazzi Bruno: 806908
Villa Giacomo: 807462
### Descrizione applicazione
L’applicazione da noi implementata è un semplice sistema di rating per un certo ristorante; è scritta in `python3` e si interfaccia con un database mysql mediante la libreria `mysql.connector`.
L’input da parte dell’utente riguarda l’inserimento del voto con l’aggiunta di tutta una serie di informazioni (nome, email, data voto). È stata implementata una serie di controlli per quanto riguarda il formato del voto e il permesso dell’utente a votare (dato una mail utente permettiamo un voto al giorno).
In seguito all’inserimento verranno mostrati tutti i voti inseriti dagli tutti gli utenti (compreso l’ultimo voto) e la media di questi.


### Descrizione architettura e obiettivi
Il nostro obiettivo è quello di avere un meccanismo di testing automatico ad ogni push; per fare questo è necessario che le parti (la nostra applicazione e il database) siano eseguite e interfacciate.

La nostra idea è quella di avere due container, uno contenente l’applicazione da noi scritta e un altro contenente il database. 
Grazie al file .gitlab-ci.yml definiamo la pipeline da eseguire ad ogni nuovo push sulla repo da parte degli sviluppatori: 
- Viene creato un container per i test case (sostanzialmente un main che esegue una serie di operazioni input simulando l’utente).
- Viene creato un container per il database (abbiamo utilizzato un’istanza di un server MySQL già presente https://hub.docker.com/_/mysql/)
- Vengono linkati i container e runnati così da iniziare ad eseguire i test da noi scritti.

Abbiamo dunque due docker, potenzialmente presenti su macchine diverse, che comunicano tra di loro in funzione dei test case da noi scritti. Alla fine dell’esecuzione, grazie a view di GitLab, abbiamo un resoconto dell’esecuzione dei test con i possibili errori. 

Andremo dunque a creare un’infrastruttura distribuita (docker separati e indipendenti) implementato le operazioni DevOps riguardanti la containerization mediante i docker e il CI/CD mediante giLab grazie al file .gitlab-ci.yml.

![Screenshot](wide_pipe.png)
